# evalhyd-cpp

Utility to evaluate deterministic and probabilistic streamflow predictions

Documentation: https://hydrogr.github.io/evalhyd/cpp
